#include "Path.h"

Path::Path() {
	head = NULL;
	tail = NULL;
}

void Path::addPos(Pose pose) {
	Node *temp = new Node;
	temp->pose = pose;
	temp->next = NULL;
	if (head == NULL) {
		head = temp;
		tail = temp;
		temp = NULL;
	}
	else {
		tail->next = temp;
		tail = temp;
	}
}

void Path::print() {
	Node *temp = new Node;
	temp = head;
	while (temp != NULL) {
		cout << "X : " << temp->pose.getX()
			<< " | Y : " << temp->pose.getY()
			<< " | TH : " << temp->pose.getTh() << endl;
		temp = temp->next;
	}
}

Pose Path::getPos(int index) {
	Node *temp = new Node;
	temp = head;
	for (int i = 1; i < index; i++) {
		if (temp == NULL) {
			Pose p;
			return p;
		}
		temp = temp->next;
	}
	return temp->pose;
}

Node* Path::getHead()const {
	return this->head;
}

bool Path::removePos(int index) {
	Node *temp = new Node;
	Node *prv = new Node;
	temp = head;
	if (index < 1)
		return false;
	if (index == 1) {
		head = head->next;
		return true;
	}

	for (int i = 1; i < index && temp != NULL; i++) {
		if (temp->next == NULL) {
			return false;
		}
		prv = temp;
		temp = temp->next;
	}
	prv->next = temp->next;
	return true;
}

bool Path::insertPos(int index, Pose pose) {
	Node *temp = new Node;
	Node *prv = new Node;
	Node *cur = new Node;
	temp->pose = pose;
	cur = head;
	for (int i = 0; i < index; i++) {
		if (cur == NULL) {
			cout << "insertpos bulunamadı" << endl;
			return false;
		}
		prv = cur;
		cur = cur->next;
	}
	prv->next = temp;
	temp->next = cur;
	return true;
}

int Path::listCount() {
	int i = 0;
	
	Node *temp = new Node;
	temp = head;
	while (temp != NULL) {
		i++;
		temp = temp->next;
	}
	return i;
}

Pose Path::operator[](int i) {
	return getPos(i);
}

ostream &operator<<(ostream &output, const Path &path) {
	Node *temp = new Node;
	temp = path.getHead();
	while (temp != NULL) {
		output << "X : " << temp->pose.getX()
			<< " Y : " << temp->pose.getY()
			<< " TH : " << temp->pose.getTh() << endl;
		temp = temp->next;
	}
	return output;
}

istream &operator >> (istream &input, Path &path) {
	float x, y, th;
	input >> x >> y >> th;
	Pose p;
	p.setX(x);
	p.setY(y);
	p.setTh(th);
	path.addPos(p);
	return input;
}