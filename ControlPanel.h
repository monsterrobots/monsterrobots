#include "PioneerRobotAPI.h"
#include "Record.h"
#include "RobotOperator.h"
#include "Pose.h"
#include "RobotControl.h"
#include "LaserSensor.h"
#include "SonarSensor.h"
#include "Path.h"
#include "PioneerRobotInterface.h"

#ifndef CONTROLPANEL_H
#define CONTROLPANEL_H

class ControlPanel
{
private:
	PioneerRobotInterface pioneerRobot;
	RobotControl robot;
	SonarSensor sonar;
	LaserSensor laser;
	Pose updatedPose;
	Pose tmpPose;
	Path path;
	Record record;
	RobotOperator robotOperator;

	string tmpName;
	int tmpPass;
	int tmpDistance;
	int choice, a, selected, loop, nloop = 0;
	float updatedSonar[16], updatedLaser[181];
	float tmpX, tmpY, tmpTh;
	float tmpMoveSpeed;


public:
	ControlPanel();
	~ControlPanel();
	void menu();
};

#endif // !CONTROLPANEL_H