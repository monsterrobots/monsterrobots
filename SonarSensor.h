/**
* @File SonarSensor.h
* @Date Aral�k, 2018
* @Author Ilkay EROL (ilkayerol3356@gmail.com)
*
* \brief SonarSensor Sinifi
* \brief Bu sinif robotun lazer sensorleri ile islem yapmasina olanak saglar.
*/

#include "PioneerRobotAPI.h"
#include "RangeSensor.h"

#ifndef SONARSENSOR_H_
#define SONARSENSOR_H_

class SonarSensor : public RangeSensor
{
private:
	/**
	* \brief ranges sensorun mesafe bilgisidir
	*/
	float ranges[16];
	/**
	* \brief angles sensorun aci bilgisidir
	*/
	float angles[16];
	PioneerRobotAPI* robotAPI;
	/**
	* \brief setAngles sensorun default degerlerini belirler
	*/
	void setAngles();
public:
	/**
	* \brief SonarSensor sensorlerin kurucu fonskiyonudur.
	*/
	SonarSensor();
	/**
	* \brief LaserSensor sensorlerin kurucu fonskiyonudur.
	*/
	SonarSensor(float[]);
	float getRange(int)const;
	float operator[](int)const;
	float getAngle(int)const;


	/**
	* \brief getMax mesafe degerlerinden maximum olani dondurur.
	*/
	float getMax(int&)const;
	/**
	* \brief getMin mesafe degerlerinden minimum olani dondurur.
	*/
	float getMin(int&)const;
	/**
	* \brief updateSensor  Robota ait guncel sensor mesafe degerlerini, ranges dizisine yukler.
	*/
	void updateSensor(float[]);
	/**
	* \brief getClosestRange En yakin mesafeyi dondurur.
	*/
	float getClosestRange(float, float, float&)const;
};
#endif // !SONARSENSOR_H_

