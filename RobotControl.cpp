/* \author	Talha KIYAK
*	152120161021
*	IDE:	Visual Studio Community 2015
*/
/* 
* \brief .cpp
*/

#include<iostream>
#include"RobotControl.h"

using namespace std;
/**
	* \brief Get to object(robotAPI)
	* \return robotAPI
	*/
/**
	* \brief Constructor
	*/
RobotControl::RobotControl(RobotInterface* robo, RangeSensor* sen1, RangeSensor* sen2, RobotOperator* robotOpo)
{
	robotInterface = robo;
	sensor1 = sen1;
	sensor2 = sen2;
	robotOperator = robotOpo;
}
/**
	* \brief Turn left Function
	*/
void RobotControl::turnLeft() {

	robotInterface->turnLeft();

}
/**
	* \brief Turn right Function
	*/
void RobotControl::turnRight() {

	robotInterface->turnRight();

}
/**
	* \brief Forward Function
	* \param speed (to contunie with given speed)
	*/
void RobotControl::forward(float speed) {

	robotInterface->forward(speed);

}
/**
	* \brief Print to position of robot
	*/
void RobotControl::print() {
	Pose pose = robotInterface->getPose();
	cout << "MyPose is (" << pose.getX() << "," << pose.getY() << "," << pose.getTh() << ")" << endl;
}
/**
	* \brief Backward function
	* \param speed (to go back with given speed)
	*/
void RobotControl::backward(float speed) {

	robotInterface->backward(speed);
}
/**
	* \brief Get function of pose
	* \return position (from Pose class)
	*/
Pose RobotControl::getPose() {

	return robotInterface->getPose();
}
/**
	* \brief Set function of pose (to PioneerRobotAPI from Pose)
	* \param p (taken from Pose class)
	*/
void RobotControl::setPose(Pose p) {
	robotInterface->setPose(p);
}
/**
	* \brief function to stop turning
	*/
void RobotControl::stopTurn() {
	robotInterface->stopTurn();
}
/**
	* \brief function to stop moving
	*/
void RobotControl::stopMove() {
	robotInterface->stopMove();
}
/**
	* \brief Destructor
	*/
RobotControl::~RobotControl(){
};
//////////////////// Kartal Added ////////////////////
bool RobotControl::openAccess(int a)
{
	if (robotOperator->checkAccessCode(a))
	{
		robotOperator->setAccessState(true);
		return true;
	}
	else
	{
		return false;
	}
}
bool RobotControl::closeAccess(int a)
{
	if (robotOperator->checkAccessCode(a))
	{
		robotOperator->setAccessState(false);
		return true;
	}
	else
	{
		return false;
	}
}
//////////////////////////////////////////////////////

//// Gokhan ADDED /////
bool RobotControl::addToPath() {
	path.addPos(robotInterface->getPose());
	return true;
}

bool RobotControl::clearPath() {
	while (path.removePos(1));
	return true;
}

bool RobotControl::recordPathToFile() {
	Pose pose;
	for (int i = 1; i <= path.listCount(); i++) {
		pose = path.getPos(i);
		record.writeLine("Robot's position (X,Y,Th) : " + to_string(pose.getX()) + "," + to_string(pose.getY()) + "," + to_string(pose.getTh()));
	}
	return true;
}
///////////////////////