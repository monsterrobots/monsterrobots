/**
* @File RangeSensor.h
* @Date Aralık, 2018
* @Author Ilkay EROL (ilkayerol3356@gmail.com)
*
* \brief RangeSensor Sinifi
* \brief Bu sinif robot icin yeni sensorler turetmeye olanak saglar. 
* \brief Olusturulacak diger sensorler bu sensoru miras olarak alirlar.
*/

#include "PioneerRobotAPI.h"
#ifndef RANGESENSOR_H_
#define RANGESENSOR_H_

class RangeSensor
{
private:

public:
	/**
	* \brief getRange fonksiyona verilen indexe sahip sensorun mesafe bilgisini dondurur.
	*/
	virtual float getRange(int)const = 0;
	/**
	* \brief operator[] indexi verilen sensordegerini dondurur
	*/
	virtual float operator[](int)const = 0;
	/**
	* \brief getAngle indexi verilen sensorun aci degerini dondurur
	*/
	virtual float getAngle(int)const = 0;
	/**
	* \brief getMax mesafe degerlerinden maximum olani dondurur.
	*/
	virtual float getMax(int&)const = 0;
	/**
	* \brief getMin mesafe degerlerinden minimum olani dondurur.
	*/
	virtual float getMin(int&)const = 0;
	/**
	* \brief updateSensor robota ait guncel sensor mesafe degerlerini, ranges dizisine yukler.
	*/
	virtual void updateSensor(float[]) = 0;
	/**
	* \brief getClosestRange iki aci arasinda kalan mesafelerden en kucuk olani angle uzerinden dondurur, mesafeyi return ile dondurur
	*/
	virtual float getClosestRange(float, float, float&)const = 0;
};


#endif // !RANGESENSOR_H_
