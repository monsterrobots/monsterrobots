
// Alperen Enes Bayar //
//	  152120171098	  //

#ifndef POSE_H
#define POSE_H

#include <math.h>


/**
	* \brief A Class for keeping coordiate values
	*/
class Pose
{
friend class RobotControl;
private:
	float x;	
	float y;
	float th;
public:

	/**
	* \brief change x
	*/
	Pose();
	/**
	* \return x
	*/
	float getX();
	/**
	* \brief change y
	*/
	void setX(float x);
	/**
	* \return y
	*/
	float getY();
	/**
	* \brief change th
	*/
	void setY(float y);
	/**
	* \return th
	*/
	float getTh();
	/**
	* \brief th
	*/
	void setTh(float th);
	/**
	* \brief operator overloading ==
	*/
	bool operator==(const Pose& pos);
	/**
	* \brief operator overloading +
	*/
	Pose operator+(const Pose& pos);
	/**
	* \brief operator overloading -
	*/
	Pose operator-(const Pose& pos);
	/**
	* \brief operator overloading +=
	*/
	Pose& operator+=(const Pose& pos);
	/**
	* \brief operator overloading -=
	*/
	Pose& operator-=(const Pose& pos);
	/**
	* \brief operator overloading <
	*/
	bool operator<(const Pose& pos);
	/**
	* \return Pose
	*/
	Pose& getPose();
	/**
	* \brief change Pose
	*/
	void setPose(float _x, float _y, float _th);
	/**
	* \brief finding distance to pos point (hypotenuse)
	*/
	float findDistanceTo(Pose pos);
	/**
	* \brief finding angle to pos point (degree)
	*/
	float findAngleTo(Pose pos);
	/**
	* \brief destructor
	*/
	~Pose();
};


#endif // !POSE_H