// Kartal Emre Buruk //
//   152120181087   //

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#ifndef RECORD_H
#define RECORD_H

/**
* \brief A class to record robot actions, riting to output file and reading from input file.
*/
class Record
{
private:
	/**
	* \brief output/input file name.
	*/
	string fileName;
	/**
	* \brief output/input file.
	*/
	fstream file;
public:
	/**
	* \brief open fstream file
	*/
	bool openFile();
	/**
	* \brief close fstream file
	*/ 
	bool closeFile();
	/**
	* \brief change fstream file name
	*/
	void setFileName(string name);
	/**
	* \brief read one line
	*/
	string readLine();
	/**
	* \brief write one line
	*/
	bool writeLine(string str);
	/**
	* \brief operator overload << for output
	*/
	friend ostream &operator<<(ostream&, const Record&);
	/**
	* \brief operator overload >> for input
	*/
	friend istream &operator >> (istream&, Record&);
};

#endif