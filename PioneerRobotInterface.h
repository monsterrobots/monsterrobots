/*
*\author	Talha KIYAK
* ID:		152120161021
* IDE:		Visual Studio 2015
*/


#pragma once
#include<iostream>
#include"RobotInterface.h"
#include"PioneerRobotAPI.h"

using namespace std;

class PioneerRobotInterface : public RobotInterface {

private:

	/**
	* \brief Definition the object
	*/
	PioneerRobotAPI* robotAPI;

public:
	
	/**
	* \brief Constructor
	*/
	PioneerRobotInterface();

	/**
	* \brief Turn left Function
	*/
	void turnLeft();

	/**
	* \brief Turn right Function
	*/
	void turnRight();

	/**
	* \brief Forward Function
	* \param speed (to contunie with given speed)
	*/
	void forward(float speed);

	/**
	* \brief Print to position of robot
	*/
	void print();

	/**
	* \brief Backward function
	* \param speed (to go back with given speed)
	*/
	void backward(float speed);

	/**
	* \brief Get function of pose
	* \return position (from Pose class)
	*/
	Pose getPose();

	/**
	* \brief Set function of pose (to PioneerRobotAPI from Pose)
	* \param p (taken from Pose class)
	*/
	void setPose(Pose);

	/**
	* \brief function to stop turning
	*/
	void stopTurn();

	/**
	* \brief function to stop moving
	*/
	void stopMove();

	/**
	* \brief update sensors function
	*/
	void updateSensors();

	/**
	* \brief Destructor
	*/
	~PioneerRobotInterface();

	bool connect();
	bool disconnect();

	void getLaserRanges(float[]);
	void getSonarRanges(float[]);

};
