#pragma once

 // alp-ene-bay    //
// 152120171098 //

#include<iostream>
#include "Pose.h"

class RobotInterface
{
	friend class PioneerRobotInterface;
private:
	Pose* position;
	int state;
public:
	/**
	* \brief Constructor
	*/
	RobotInterface();
	/**
	* \brief Turn left Function
	*/
	virtual void turnLeft() = 0;

	/**
	* \brief Turn right Function
	*/
	virtual void turnRight() = 0;

	/**
	* \brief Forward Function
	* \param speed (to contunie with given speed)
	*/
	virtual void forward(float speed) = 0;

	/**
	* \brief Print to position of robot
	*/
	virtual void print() = 0;

	/**
	* \brief Backward function
	* \param speed (to go back with given speed)
	*/
	virtual void backward(float speed) = 0;

	/**
	* \brief Get function of pose
	* \return position (from Pose class)
	*/
	virtual Pose getPose() = 0;

	/**
	* \brief Set function of pose (to PioneerRobotAPI from Pose)
	* \param p (taken from Pose class)
	*/
	virtual void setPose(Pose) = 0;

	/**
	* \brief function to stop turning
	*/
	virtual void stopTurn() = 0;

	/**
	* \brief function to stop moving
	*/
	virtual void stopMove() = 0;
	/**
	* \brief function to stop moving
	*/
	virtual void updateSensors() = 0;

	/**
	* \brief Destructor
	*/
	~RobotInterface();
};

