#include "Pose.h"
#include <iostream>

using namespace std;

int main(void)
{

	Pose a,b,c;
	bool d;
	a.setPose(1, 2, 20);
	b.setPose(4, 6, 50);

	cout << a.findDistanceTo(b) << endl;
	cout << a.findAngleTo(b) << endl;
	d = (a==b);
	cout << "a==b isTrue -> "<< d << endl;
	d = (a < b);
	cout << "a<b isTrue -> "  << d << endl << endl;

	cout << "a -> " <<a.getX() << " " << a.getY() << " " << a.getTh() << endl;
	cout << "b -> "<<b.getX() << " " << b.getY() << " " << b.getTh() << endl;
	cout << "a+b = ";
	c = a + b;
	cout << c.getX() << " " << c.getY() << " " << c.getTh() << endl;
	
	
	cout << "a -> " << a.getX() << " " << a.getY() << " " << a.getTh() << endl;
	cout << "b -> " << b.getX() << " " << b.getY() << " " << b.getTh() << endl;
	a += b;
	cout << "a+=b -> "<< a.getX() << " " << a.getY() << " " << a.getTh() << endl;


	
	system("pause");
	return 0;
}