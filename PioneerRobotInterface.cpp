/*
*\author	Talha KIYAK
* ID:		152120161021
* IDE:		Visual Studio 2015
*/

#include<iostream>
#include"PioneerRobotInterface.h"

using namespace std;

/**
* \brief Constructor
*/
PioneerRobotInterface::PioneerRobotInterface()
{
	/**
	* \brief form the object
	*/
	robotAPI = new PioneerRobotAPI();
	
}
/**
* \brief Turn left Function
*/
void PioneerRobotInterface::turnLeft() {

	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::left);

}
/**
* \brief Turn right Function
*/
void PioneerRobotInterface::turnRight() {

	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::right);

}
/**
* \brief Forward Function
* \param speed (to contunie with given speed)
*/
void PioneerRobotInterface::forward(float speed) {

	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
	robotAPI->moveRobot(speed);

}
/**
* \brief Print to position of robot
*/
void PioneerRobotInterface::print() {
	cout << "MyPose is (" << position->getX() << "," << position->getY() << "," << position->getTh() << ")" << endl;
}
/**
* \brief Backward function
* \param speed (to go back with given speed)
*/
void PioneerRobotInterface::backward(float speed) {

	float _th1;
	Pose p, tempPose;
	tempPose.setPose(robotAPI->getX(), robotAPI->getY(), robotAPI->getTh());
	p.setTh(tempPose.getTh());

	_th1 = tempPose.findAngleTo(p);
	_th1 += 180;
	tempPose.setTh(robotAPI->getTh());
	robotAPI->setPose(tempPose.getX(), tempPose.getY(), tempPose.getTh());
	robotAPI->moveRobot(speed);

}
/**
* \brief Get function of pose
* \return position (from Pose class)
*/
Pose PioneerRobotInterface::getPose() {
	Pose tempPose;
	tempPose.setPose(robotAPI->getX(), robotAPI->getY(), robotAPI->getTh());
	return tempPose;
}
/**
* \brief Set function of pose (to PioneerRobotAPI from Pose)
* \param p (taken from Pose class)
*/
void PioneerRobotInterface::setPose(Pose p) {
	float X1, Y1, TH1;
	X1 = p.getX();
	Y1 = p.getY();
	TH1 = p.getTh();
	robotAPI->setPose(X1, Y1, TH1);
}
/**
* \brief function to stop turning
*/
void PioneerRobotInterface::stopTurn() {
	robotAPI->turnRobot(PioneerRobotAPI::DIRECTION::forward);
}
/**
* \brief function to stop moving
*/
void PioneerRobotInterface::stopMove() {
	robotAPI->stopRobot();
}

void PioneerRobotInterface::updateSensors()
{

}
/**
* \brief Destructor
*/
PioneerRobotInterface::~PioneerRobotInterface() {
	delete robotAPI;
	delete position;
}

bool PioneerRobotInterface::connect() {
	return robotAPI->connect();
}

bool PioneerRobotInterface::disconnect() {
	return robotAPI->disconnect();
}

void PioneerRobotInterface::getLaserRanges(float ranges[]) {
	robotAPI->getLaserRange(ranges);
}

void PioneerRobotInterface::getSonarRanges(float ranges[]) {
	robotAPI->getSonarRange(ranges);
}