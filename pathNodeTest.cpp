/**********************************
* pathNodeTest.cpp                *
***********************************
* IDE : Visual Studio 2015        *
* Author : Gokhan OZBEY           *
* Brief : Nesne Guz Donem Projesi *
**********************************/

#include "Path.h"

int main() {
	Path path;
	Pose p;
	p.setX(32);
	p.setY(4);
	p.setTh(12321);
	path.addPos(p);
	Pose p2;
	p2.setX(11);
	p2.setY(24);
	p2.setTh(321);
	path.addPos(p2);
	Pose p3;
	p3.setX(99);
	p3.setY(49);
	p3.setTh(9999);
	path.insertPos(1, p3);
	path.print();
	path.removePos(6);
	cout << endl;
	path.print();
	cout << endl;
	path.addPos(path[3]);
	cout << path;
	cin >> path;
	cout << path;

	system("pause");
	return 0;
}