/**
*\author Talha KIYAK
*	152120161021
*	IDE:	Visual Studio Community 2015
*
*\brief A class to control of robot
*/

#include<iostream>
#include "RobotOperator.h"
#include "Record.h"
#include "Path.h"
#include "RangeSensor.h"
#include "RobotInterface.h"

using namespace std;

#ifndef ROBOT_CONTROL_H
#define ROBOT_CONTROL_H

class RobotControl {

private:
	int state;
	//////////////////////////////
	RobotOperator *robotOperator;
	Record record;
	Path path;
	RangeSensor *sensor1;
	RangeSensor *sensor2;
	RobotInterface *robotInterface;
public:
	/**
	* \brief Constructor
	*/
	RobotControl(RobotInterface*, RangeSensor*, RangeSensor*, RobotOperator*);

	/**
	* \brief Turn left Function
	*/
	void turnLeft();

	/**
	* \brief Turn right Function
	*/
	void turnRight();

	/**
	* \brief Forward Function
	* \param speed (to contunie with given speed)
	*/
	void forward(float speed);

	/**
	* \brief Print to position of robot
	*/
	void print();

	/**
	* \brief Backward function
	* \param speed (to go back with given speed)
	*/
	void backward(float speed);

	/**
	* \brief Get function of pose
	* \return position (from Pose class)
	*/
	Pose getPose();

	/**
	* \brief Set function of pose (to PioneerRobotAPI from Pose)
	* \param p (taken from Pose class)
	*/
	void setPose(Pose);

	/**
	* \brief function to stop turning
	*/
	void stopTurn();

	/**
	* \brief function to stop moving
	*/
	void stopMove();

	/**
	* \brief Destructor
	*/
	~RobotControl();

	/////////////// Kartal Added ///////////////
	/**
	* \brief function to open access to robot if the access code is true.
	*/
	bool openAccess(int);
	/**
	* \brief function to close access to robot if the access code is true.
	*/
	bool closeAccess(int);
	////////////////////////////////////////////

	//// Gokhan ADDED /////
	/**
	* \brief function to add objects to path.
	*/
	bool addToPath();
	/**
	* \brief function to clear path objects.
	*/
	bool clearPath();
	/**
	* \brief function to write path to output file.
	*/
	bool recordPathToFile();
	///////////////////////
};

#endif		//ROBOT_CONTROL_H
