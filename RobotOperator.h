// Kartal Emre Buruk //
//   152120181087   //

#include "Encryption.h"
#include <iostream>
#include <string>

using namespace std;

#ifndef ROBOTOPERATOR_H
#define ROBOTOPERATOR_H

/**
* \brief A Class to encryption/decryption given value, check accessState and print all.
*/
class RobotOperator
{
private:
	Encryption tmp;
	string tmpState;
	/**
	* \brief name of user
	*/
	string name;
	/**
	* \brief surname of user
	*/
	string surname;
	/**
	* \brief accessCode to check
	*/
	unsigned int accessCode;
	/**
	* \brief accessState of user
	*/
	bool accessState;
	/**
	* \brief encrypt given int with encryption function of encryption
	*/
	int encryptCode(int);
	/**
	* \brief decrypt given int with decryption function of encryption
	*/
	int decryptCode(int);
public:
	/**
	* \brief check given int if its the same with accessCode
	*/
	bool checkAccessCode(int);
	/**
	* \brief print name,surname,accessState of user
	*/
	void print();
	///////////////////////////////////////////// Added /////////////////////////////////////////////
	/**
	* \brief set AccessState to lock/unlock robot's functions
	*/
	void setAccessState(bool);
	/**
	* \brief check AccessState if its true or false
	*/
	bool checkAccessState();
	/**
	* \brief set user name,surname and accessCode
	*/
	void setUser(string, string, int);
	/////////////////////////////////////////////////////////////////////////////////////////////////
};

#endif