// Kartal Emre Buruk //
//   152120181087   //

#include "Record.h"

bool Record::openFile()
{
	file.open(fileName, ios::out | ios::in | ios::trunc);
	if (file.is_open())
	{
		return true;
	}
	else
	{
		return false;
	}
}
bool Record::closeFile()
{
	file.close();
	if (file.is_open())
	{
		return false;
	}
	else
	{
		return true;
	}
}
void Record::setFileName(string name)
{
	fileName = name;
}
int i = 0; // hangi satiri alacagimizi ogrenmek icin
string Record::readLine()
{	
	string str;
	file.seekg(i, ios::beg);
	getline(file, str);
	int x = str.length(); // son alinan satirin str uzunlugu
	i = i + x + 2;  // sonraki line'a gecis icin
	return str;
}
bool Record::writeLine(string str)
{
	file.seekg(0,ios_base::end);
	file << str << endl;
	return true;
}
ostream &operator<<(ostream& out, const Record& a)
{
	out << a;
	return out;
}
istream &operator >> (istream& in, Record& a)
{
	in >> a;
	return in;
}
