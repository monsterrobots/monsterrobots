#include "Node.h"
#include <iostream>

/**********************************
* Path.h                          *
***********************************
* IDE : Visual Studio 2015        *
* Author : Gokhan OZBEY           *
* Brief : Nesne Guz Donem Projesi *
**********************************/

using namespace std;

#ifndef PATH_H_
#define PATH_H_
/**
*\brief Konumlari bir listede tutar. Liste uzerinde silme ekleme ve degistirme gibi degisiklikler yapilabilir.
*/
class Path {
public:
	/**
	*\brief Constructor. Pointerlari NULL'a esitler
	*/
	Path();

	/**
	*\brief Verilen pos konumunun listenin sonuna ekler.
	*\param pose(Pose)
	*/
	void addPos(Pose pose);

	/**
	*\brief Listede bulunan konumlari, duzgun bir formatta ekrana yazdirir
	*/
	void print();

	/**
	*\brief Verilen indeksteki konumu dondurur
	*\param index(int)
	*/
	Pose getPos(int index);

	/**
	*\brief Listenin basini tutan head pointerini dondurur
	*/
	Node* getHead()const;

	/**
	*\brief Verilen indeksteki konumu listeden siler
	*\param index(int)
	*/
	bool removePos(int index);

	/**
	*\brief Verilen indeksten sonraya verilen konumu ekler
	*\param index(int)
	*\param pose(Pose)
	*/
	bool insertPos(int index, Pose pose);

	int listCount();

	/**
	*\brief Verilen indeksteki konumu dondurur
	*\param i(int)
	*/
	Pose operator[](int i);

	/**
	*\brief Verilen pathin sahip oldugu listedeki elemanlari yazdirir
	*\output(ostream&)
	*\path(Path&)
	*/
	friend ostream &operator<<(ostream &output, const Path &path);

	/**
	*\brief Verilen path classinin sahip oldugu listenin sonuna yeni konum ekler
	*\input(istream&)
	*\path(Path&)
	*/
	friend istream &operator>> (istream &input, Path &path);

private:
	Node* head; //Listenin basini tutan pointer
	Node* tail; //Listenin sonunu tutan pointer
	int number;
};

#endif // !PATH_H_
