/**
* @File LaserSensor.h
* @Date Aral�k, 2018
* @Author Ilkay EROL (ilkayerol3356@gmail.com)
*
* \brief Lasersensor Sinifi
* \brief Bu sinif robotun lazer sensorleri ile islem yapmasina olanak saglar.
*/
#include "PioneerRobotAPI.h"
#include "RangeSensor.h"

#ifndef LASERSENSOR_H_
#define LASERSENSOR_H_

class LaserSensor : public RangeSensor
{
private:
	/**
	* \brief ranges sensorun mesafe bilgisidir
	*/
	float ranges[181];
	/**
	* \brief angles sensorun aci bilgisidir
	*/
	float angles[181];
	/**
	* \brief setAngles sensorun default degerlerini belirler
	*/
	void setAngles();
	PioneerRobotAPI* robotAPI;
public:
	/**
	* \brief LaserSensor sensorlerin kurucu fonskiyonudur.
	*/
	LaserSensor();
	/**
	* \brief LaserSensor() sensorlerin kurucu fonksiyonudur.
	*/
	LaserSensor(float[]);
	/**
	* \brief  getMax mesafe degerlerinden maximum olani dondurur.
	*/
	float getRange(int)const;
	float operator[](int)const;
	float getAngle(int)const;


	float getMax(int&)const;
	/**
	* \brief getMin mesafe degerlerinden minimum olani dondurur.
	*/
	float getMin(int&)const;

	/**
	* \brief updateSensor  Robota ait guncel sensor mesafe degerlerini, ranges dizisine yukler. 
	*/
	void updateSensor(float[]);
	/**
	* \brief getClosestRange iki aci arasinda kalan mesafelerden en kucuk olani angle uzerinden dondurur, mesafeyi return ile dondurur.
	*/
	float getClosestRange(float, float, float&)const;
};
#endif // !LASERSENSOR_H_


