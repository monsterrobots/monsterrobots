/**********************************
* Node.h                          *
***********************************
* IDE : Visual Studio 2015        *
* Author : Gokhan OZBEY           *
* Brief : Nesne Guz Donem Projesi *
**********************************/

#include "Pose.h"
/**
*\brief Konumu tutan bir dugum gorevi gorur. Listeyi olusturmak icin kullanilir.
*/
#ifndef NODE_H_
#define NODE_H_
class Node {
public:
	Node* next;
	Pose pose;
};

#endif // !NODE_H_
