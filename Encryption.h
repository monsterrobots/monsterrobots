// Kartal Emre Buruk //
//   152120181087   //

#ifndef ENCRYPTION_H
#define ENCRYPTION_H

/**
* \brief A Class to Encrypt and Decrypt given values.
*/
class Encryption
{
public:
	/**
	* \brief encrypt given int
	*/
	int encrypt(int);
	/**
	* \brief decrypt given int
	*/
	int decrypt(int);
};

#endif