// Kartal Emre Buruk //
//   152120181087   //

#include "RobotOperator.h"

int RobotOperator::encryptCode(int a)
{
	return tmp.encrypt(a);
}
int RobotOperator::decryptCode(int a)
{
	return tmp.decrypt(a);
}
bool RobotOperator::checkAccessCode(int a)
{
	if (tmp.encrypt(a) == accessCode)
	{
		return true;
	}
	else
	{
		return false;
	}
}
void RobotOperator::print()
{
	if (accessState == true)
	{
		tmpState = "True";
	}
	else
	{
		tmpState = "False";
	}

	cout << "Name Surname : " << name << " " << surname << endl;
	cout << "Access state of user : " << tmpState << endl;
}
//////////////////// Added ////////////////////
void RobotOperator::setAccessState(bool a)
{
	if (a == true)
	{
		accessState = true;
	}
	if (a == false)
	{
		accessState = false;
	}
}
bool RobotOperator::checkAccessState()
{
	return accessState;
}
void RobotOperator::setUser(string a, string b, int c)
{
	name = a;
	surname = b;
	accessCode = tmp.encrypt(c);
}
///////////////////////////////////////////////